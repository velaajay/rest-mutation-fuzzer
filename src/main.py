from keys import env_keys
from utils import get_config
from fuzzingbook.MutationFuzzer import MutationFuzzer
from rest_helper import handle_post
import sys
from prettytable import PrettyTable
from junit_xml import TestSuite, TestCase, to_xml_report_file
from fuzzing.fuzzingreport import MutationFuzzerReport as Report
import json
import os

HTTP = 'http://'
PORT_SEPERATOR = ':'
SLASH = '/'
TEST = '.test'
DOT = '.'
REPORT_DIR = 'report/'
REPORT_NAME = 'junit_fuzzing_report.xml'


def please_agreegate(api_name, api_test_agreegation, result):
    if api_name in api_test_agreegation:
        if result['status_code'] in api_test_agreegation[api_name]:
            pointer_to_dict = api_test_agreegation[api_name]
            pointer_to_dict[result['status_code']] += 1
        else:
            pointer_to_dict = api_test_agreegation[api_name]
            pointer_to_dict[result['status_code']] = 1
    else:
        pointer_to_dict = dict()
        api_test_agreegation[api_name] = pointer_to_dict
        pointer_to_dict[result['status_code']] = 1


def make_preety_table(api_test_agreegation):
    table = PrettyTable(border=True, header=True, padding_width=1,
                        field_names=["API Name", "Status Code", "Count"])

    for api_name, result_dict in api_test_agreegation.items():
        for status_code, count in result_dict.items():
            table.add_row([api_name, status_code, count])

    print(table)
    sys.stdout.flush()


def dump_test_case(test_cases: list, report: Report):
    if report.get_status() != Report.PASSED:
        test_case = TestCase(name=report.get_name(), status=report.get_status())
        test_case.add_failure_info(message=json.dumps(report.to_dict()))
        test_cases.append(test_case)
    else:
        test_case = TestCase(name=report.get_name(), status='Pass', stdout=json.dumps(report.to_dict()))
        test_cases.append(test_case)


def save_report( test_cases):

    try:
        if not os.path.exists(os.path.dirname(REPORT_DIR)):
            try:
                os.makedirs(os.path.dirname(REPORT_DIR))
            except OSError:
                pass
        with open(REPORT_DIR + REPORT_NAME , 'w') as report_file:
            to_xml_report_file(report_file, [TestSuite("Mutation Fuzzer - Test Suite", test_cases)], prettyprint=True)
            # to_xml_report_file(report_file, [TestSuite("Mutation Fuzzer - Passed", passed_test_cases)], prettyprint=True)
    except Exception as e:
        print('Failed to save report "{}" to {} because: {}'
              .format(REPORT_NAME, REPORT_DIR, e))


"""
Attaching a fuzzer to each object from the input. Seed input is parsed from the input file and passed to the fuzzer.
"""


def prepare_fuzzer(seed_input):
    fuzzer_map = dict()
    for k, v in seed_input.items():
        mutation_fuzzer = MutationFuzzer(seed=[str(v)])
        fuzzer_map[k] = mutation_fuzzer

    return fuzzer_map


def prepare_request(fuzzers_map: dict):
    request_data = dict()
    for k, v in fuzzers_map.items():
        request_data[k] = v.fuzz()
    return request_data


def main():
    # Load the config and seed input from the file.
    apis_test_config = get_config()
    rest_container_url = HTTP + env_keys['REST_HOST'] + PORT_SEPERATOR + env_keys['REST_PORT']
    api_test_agreegation = dict()
    test_cases = list()

    # Loop over each API defined in the config.json and start fuzzing the API and capture the output.
    for api in apis_test_config:
        seed_input = api.get_seed()
        fuzzers = prepare_fuzzer(seed_input)
        url = rest_container_url + SLASH + api.get_name()

        for idx in range(env_keys['MAX_MUTATIONS']):
            test_case_name = api.get_name() + DOT + str(idx) + TEST
            report = Report(test_case_name)
            data = prepare_request(fuzzers)
            result = handle_post(url, data, timeout=env_keys['API_TIME_OUT'], report_obj=report)
            report.add('test_case_name', test_case_name)
            # dumping the data to prepare the report.
            dump_test_case(test_cases, report)
            # Aggregate the numbers into
            please_agreegate(api_name=api.get_name(), api_test_agreegation=api_test_agreegation, result=result)

    # Make it to print to docker console.
    make_preety_table(api_test_agreegation=api_test_agreegation)

    # Save the test cases status to a report in junit format so that it can be used in Gitlab
    save_report(test_cases)

    # This is added to stop the container, where it will wait for ctrl+c or ctrl+d
    while True:
        try:
            line = sys.stdin.readline()
            if line == "":
                break
        except KeyboardInterrupt as e:
            print("Received a termination signal, program will exit shortly")
            break
        except Exception as e:  # This is written to make sure any unhandled error should not crash the program.
            print("Error: Some generic error : {0}".format(line), file=sys.stderr)
            break

    # return exit code 0 on successful termination
    sys.exit(0)


if __name__ == '__main__':
    main()
