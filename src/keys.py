import os
import sys

env_keys = dict()

# Mandatory environment variables.
try:
    env_keys['REST_HOST'] = os.environ['REST_HOST']
    env_keys['REST_PORT'] = os.environ['REST_PORT']
except KeyError as e:
    print("Please set all the required environment variables {0}".format(e), file=sys.stderr)
    sys.exit(1)

# Optional variables, if not passed
if 'MIN_MUTATIONS' in os.environ:
    env_keys['MIN_MUTATIONS'] = os.environ['MIN_MUTATIONS']
else:
    env_keys['MIN_MUTATIONS'] = 2

if 'MAX_MUTATIONS' in os.environ:
    env_keys['MAX_MUTATIONS'] = int(os.environ['MAX_MUTATIONS'])
else:
    env_keys['MAX_MUTATIONS'] = 10


if 'API_TIME_OUT' in os.environ:
    env_keys['API_TIME_OUT'] = int(os.environ['API_TIME_OUT'])
else:
    env_keys['API_TIME_OUT'] = 5

