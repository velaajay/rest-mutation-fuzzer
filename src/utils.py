import json
import sys

CONFIG_FILE = "seed_input/config.json"


class Config:
    def __init__(self, api_data):
        self.name = api_data['name']
        self.operation = api_data['operation']
        self.seed = api_data['seed']

    def get_name(self):
        return self.name

    def get_operation(self):
        return self.operation

    def get_seed(self):
        data = None
        try:
            seed_file = open(self.seed)
            data = json.load(seed_file)
            seed_file.close()
        except FileNotFoundError as e:
            print("Failed in reading the seed input from file {0}".format(self.seed))
            sys.exit(1)

        return data


def get_config() -> list:
    api_config_list = list()
    try:
        config_file = open(CONFIG_FILE)
        data = json.load(config_file)

        for api in data['apis']:
            api_config_list.append(Config(api))
        config_file.close()
    except FileNotFoundError as e:
        print("Please check the config file {0} is present".format(CONFIG_FILE))
        sys.exit(1)

    return api_config_list


# Just for testing
if __name__ == '__main__':
    api_configs = get_config()
    for config in api_configs:
        print(config.get_seed())
