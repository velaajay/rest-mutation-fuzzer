from fuzzingbook.Fuzzer import Runner
from fuzzingbook.Coverage import Coverage, population_coverage

class RestAPIRunner(Runner):
    def __init__(self, api):
        self.api = api

    def run_api(self, input):
        return self.api

    def run(self, input):
        try:
            result = self.run_api(input)
            outcome = self.PASS
        except Exception:
            result = None
            outcome = self.FAIL

        return result, outcome

class RestAPICoverageRunner(RestAPIRunner):
    def run_api(self, inp):
        with Coverage() as cov:
            try:
                result = super().run_api(inp)
            except Exception as exc:
                self._coverage = cov.coverage()
                raise exc

        self._coverage = cov.coverage()
        return result

    def coverage(self):
        return self._coverage