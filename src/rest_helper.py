import requests
import json
from fuzzing.fuzzingreport import MutationFuzzerReport as Report


def handle_get(get_url):
    if get_url is None:
        return
    try:
        response = requests.get(get_url, timeout=5)
        response.raise_for_status()
        print(response.json())
    except requests.exceptions.HTTPError as errh:
        print(errh)
    except requests.exceptions.ConnectionError as errc:
        print(errc)
    except requests.exceptions.Timeout as errt:
        print(errt)
    except requests.exceptions.RequestException as err:
        print(err)


def handle_post(url, data, timeout, report_obj:Report) -> dict:
    result = {
        'response' : None,
        'error' : None,
        'status_code' : None
    }

    report_obj.set_status(Report.PASSED)

    if url is None:
        return
    try:
        response = requests.post(url=url, data=json.dumps(data),
                                 headers={'Content-type': 'application/json', 'Accept': 'text/plain'},
                                 timeout=timeout)
        report_obj.add('status_code',response.status_code )
        result['status_code'] = response.status_code
        report_obj.add('response_text', response.json())
        report_obj.add('data_payload', json.dumps(data))
        result['response'] = response
        response.raise_for_status()
    except requests.exceptions.HTTPError as errh:
        result['error'] = errh
        report_obj.add('error', errh.__str__())
        report_obj.set_status(Report.FAILED)
        # print(errh)
    except requests.exceptions.ConnectionError as errc:
        result['error'] = errc
        report_obj.add('error', errc.__str__())
        report_obj.set_status(Report.FAILED)
        # print(errc)
    except requests.exceptions.Timeout as errt:
        result['error'] = errt
        report_obj.add('error', errt.__str__())
        report_obj.set_status(Report.FAILED)
        # print(errt)
    except requests.exceptions.RequestException as err:
        result['error'] = err
        report_obj.add('error', err.__str__())
        report_obj.set_status(Report.FAILED)

    return result
